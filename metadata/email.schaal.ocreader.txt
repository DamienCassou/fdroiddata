Categories:Reading,Internet
License:GPLv3+
Author Name:Daniel Schaal
Author Email:daniel@schaal.email
Web Site:
Source Code:https://github.com/schaal/ocreader
Issue Tracker:https://github.com/schaal/ocreader/issues
Changelog:https://github.com/schaal/ocreader/blob/HEAD/CHANGELOG.md

Auto Name:OCReader
Summary:Reader for an ownCloud News instance
Description:
This app allows synchronization with an [https://github.com/owncloud/news
ownCloud News] instance.

Its aim is to be fast and simple, providing only basic reading functionality,
for the official and more fully featured client see
[[de.luhmer.owncloudnewsreader]].
.

Repo Type:git
Repo:https://github.com/schaal/ocreader

Build:0.1a,1
    commit=0.1a
    subdir=app
    gradle=yes

Build:0.1b,2
    commit=v0.1b
    subdir=app
    gradle=yes

Build:0.1b2,3
    commit=v0.1b2
    subdir=app
    gradle=yes

Build:0.1,4
    commit=v0.1
    subdir=app
    gradle=yes

Build:0.2,5
    commit=v0.2
    subdir=app
    gradle=yes

Build:0.3,6
    commit=v0.3
    subdir=app
    gradle=yes

Build:0.4,7
    commit=v0.4
    subdir=app
    gradle=yes

Build:0.5,8
    commit=v0.5
    subdir=app
    gradle=yes

Build:0.6,9
    commit=v0.6
    subdir=app
    submodules=yes
    gradle=yes

Build:0.7,10
    commit=v0.7
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.7
Current Version Code:10
