Categories:Time
License:MIT
Web Site:http://trikita.co
Source Code:https://github.com/trikita/talalarmo
Issue Tracker:https://github.com/trikita/talalarmo/issues

Auto Name:Talalarmo
Summary:Minimal, simple and beautiful alarm clock
Description:
Minimal, simple and beautiful alarm clock thoughtfully designed by nap
enthusiasts. It does only one function but does it well. Only one alarm time is
supported and it's recurring daily. Setting the new alarm time takes less than a
second. To dismiss the alarm you can touch the screen anywhere which is handy if
you are still sleepy and have barely opened your eyes. There is no snooze mode
because you should be honest to yourself: if it's time to wake up - go ahead!
.

Repo Type:git
Repo:https://github.com/trikita/talalarmo

Build:3.5,15
    commit=3.5
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:3.5
Current Version Code:15
